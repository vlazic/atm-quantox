;(function() {
  // Google API key used for:
  // - Google StaticMap API
  // - Google Maps API
  // - Google Places API
  var api_key = 'AIzaSyDc3SnWSKl66Ujb9rIT-ugIM-1WxiVyzAE';

  // create URL for map image with locations of user and atm machine
  // we are using Google StaticMap API
  var google_map_image = function(you, atm) {
    var url = 'https://maps.googleapis.com/maps/api/staticmap?';

    // get map image relative to screen size
    var img_width = document.getElementsByClassName('container')[0].offsetWidth - 30;
    var img_height = Math.round(img_width * 0.75);

    url += '&size=' + img_width + 'x' + img_height;
    url += '&maptype=roadmap';
    url += '&markers=color:green%7Clabel:Y%7C' + you.latitude + ',' + you.longitude;
    url += '&markers=color:red%7Clabel:A%7C' + atm.latitude + ',' + atm.longitude;
    url += '&key=' + api_key;

    return url;
  };

  // This function calculates the distance between two points (given the
  // latitude/longitude of those points).
  //
  // Source: http://www.geodatasource.com/developers/javascript
  var distance = function(a, b) {
    var radlat1 = Math.PI * a.latitude / 180;
    var radlat2 = Math.PI * b.latitude / 180;
    var theta = a.longitude - b.longitude;
    var radtheta = Math.PI * theta / 180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);;

    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;

    return Math.round(dist * 1609.344);
  };

  // sort distances in ascending order
  function sort_atms_asc(a, b) {
    if (a.distance < b.distance)
      return -1;
    if (a.distance > b.distance)
      return 1;
    return 0;
  }

  // sort distances in descending order
  function sort_atms_desc(a, b) {
    if (a.distance < b.distance)
      return 1;
    if (a.distance > b.distance)
      return -1;
    return 0;
  }

  var display_atms = function(atms) {
    var atms_html = '';
    var atms_el = document.getElementById('atms');
    var sorting_order = document.querySelector('input[name=sort]:checked').value;

    // sort list of atms
    if (sorting_order === 'asc') {
      atms.sort(sort_atms_asc);
    } else {
      atms.sort(sort_atms_desc);
    }

    // after sorting limit number of atms to 10
    atms = atms.slice(0, 10);

    // generate HTML
    for (var i = 0; i < atms.length; i++) {
      var atm = atms[i];
      var el = '';

      el += '<li>';
      el += '<h3>' + atm.name + '</h3>';
      el += '<img src="' + atm.map + '" alt="' + atm.address + '" class="img-thumbnail">';
      el += '<dl class="dl-horizontal">';
      el += '<dt>Distance</dt>';
      el += '<dd>' + atm.distance + ' m</dd>';
      el += '<dt>Address</dt>';
      el += '<dd>' + atm.address + '</dd>';
      el += '</dl>';
      el += '</li>';

      atms_html += el;
    }

    // add html to list
    atms_el.innerHTML = atms_html;

    // scroll to list
    window.scrollTo(0, atms_el.offsetTop - 20)
  };


  // This function is used by locate_atms function
  //
  // get atm location. this also needs to be promise because
  // geometry.location.lat and geometry.location.lng are async methods
  var get_location = function(atm, user_location) {
    return new Promise(function(resolve, reject) {
      atm.location = {
        latitude: atm.geometry.location.lat(),
        longitude: atm.geometry.location.lng()
      };
      resolve({
        atm: atm,
        user_location: user_location
      });
    });
  };

  // This function is used by locate_atms function
  //
  // construct final object for atm
  var get_atm = function(args) {
    return new Promise(function(resolve, reject) {
      resolve({
        place_id: args.atm.place_id,
        name: args.atm.name,
        address: args.atm.vicinity,
        location: args.atm.location,
        distance: distance(args.user_location, args.atm.location),
        map: google_map_image(args.user_location, args.atm.location)
      })
    });
  };

  // when user clicks on 'Find ATMs!' button send request
  // to Google Places service and display response
  var locate_atms = function(user_location) {

    var radius = document.getElementById('radius').value;
    var bank_name = document.getElementById('bank_name').value || '';

    // construct query for google places service
    var request = {
      location: {
        lat: user_location.coords.latitude,
        lng: user_location.coords.longitude
      },
      type: ['atm'],
      radius: radius,
      name: bank_name
    };

    // As documented the PlacesService accepts as argument either a map
    // or an node where to render the attributions for the results.
    // https://stackoverflow.com/a/14345546
    var service = new google.maps.places.PlacesService(document.createElement('div'));

    // fetch JSON from Google places service
    service.nearbySearch(request, function(results, status) {

      if (status == google.maps.places.PlacesServiceStatus.OK) {
        var promises = [];

        for (var i = 0; i < results.length; i++) {
          var atm = results[i];

          // for each atm from list of atms add chain of promises (get_location and get_atm)
          // to list of all promises
          //
          promises.push(get_location(atm, user_location.coords).then(get_atm))
        }

        // wait for all promises to finish and then generate HTML list
        // each promise is
        Promise
          .all(promises)
          .then(display_atms);

      } else {
        display_error('No atms nearby!');
      }
    });

  };

  // show popup for all posible geoloacation api errors
  var display_geolocation_errors = function(error) {
    if (error.code == error.PERMISSION_DENIED) {
      display_error('You denied access to your location. We can\'t continue until you unblock our app.');
    } else if (error.code == error.POSITION_UNAVAILABLE) {
      display_error('Are you sure you enabled GPS sensor? Or your device does not have it.');
    } else if (error.code == error.TIMEOUT) {
      display_error('Position retrieval timed out. Find better spot and try again.');
    }
  };

  // handle click on 'Find ATMs!' button
  var find_atms_button_click = function() {
    // check if geolocation api is supported
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(locate_atms, display_geolocation_errors);
    } else {
      display_error('Geolocation is not supported by this browser.');
    }
  }


  // display error popup
  // we are using simple wrapper around bootstrap modal:
  // http://getbootstrap.com/javascript/#modals
  var display_error = function(error) {
    var message_el = document.querySelector('#error-popup .modal-body p');

    message_el.innerHTML = error;
    $('#error-popup').modal('show');
  }

  var find_atms_button = document.getElementsByClassName('find-atms')[0];
  find_atms_button.addEventListener('click', find_atms_button_click);

})();

